## lokal

```
. corona/bin/activate
cd rohdatenanalyse
jupyter notebook
```

## Google Cloud Plattform

```
gcloud builds submit --tag gcr.io/clear-corona/rki-diagrams
gcloud run deploy --image gcr.io/clear-corona/rki-diagrams
```

